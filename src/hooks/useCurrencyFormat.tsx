import { useNumberFormat } from 'hooks/useNumberFormat'
import { NumberFormatProps } from 'react-number-format'

export function getCurrencyFormat({ locale, currency }) {
  return {
    suffix: locale === 'en' ? undefined : ` ${currency}`,
    prefix: locale === 'en' ? `${currency} ` : undefined,
  }
}

export const useCurrencyFormat = ({
  decimalScale = 2,
  fixedDecimalScale = true,
  currency = '€',
} = {}): Partial<NumberFormatProps> => {
  const numberFormat = useNumberFormat({
    decimalScale,
    fixedDecimalScale,
  })
  const currencyFormat = getCurrencyFormat({ locale: 'fr', currency })
  return {
    ...numberFormat,
    ...currencyFormat,
  }
}
