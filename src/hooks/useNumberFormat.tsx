/* eslint-disable no-nested-ternary */
import { NumberFormatProps } from 'react-number-format'

export function getNumberFormat({ locale }) {
  return {
    thousandSeparator:
      locale === 'fr'
        ? ' '
        : locale === 'de' ||
          locale === 'ca' ||
          locale === 'es' ||
          locale === 'da'
        ? '.'
        : locale === 'en'
        ? ','
        : "'",
    decimalSeparator: locale === 'en' ? '.' : ',',
  }
}

export const useNumberFormat = ({
  decimalScale = 2,
  fixedDecimalScale = true,
} = {}): Partial<NumberFormatProps> => {
  const numberFormat = getNumberFormat({ locale: 'fr' })
  return {
    decimalScale,
    allowEmptyFormatting: true,
    allowedDecimalSeparators: [',', '.'],
    fixedDecimalScale,
    ...numberFormat,
  }
}
