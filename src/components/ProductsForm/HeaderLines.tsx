import React from 'react'

import { SimpleGrid, Heading } from '@chakra-ui/react'

const HeaderLines: React.FC<{}> = () => (
  <SimpleGrid templateColumns="2fr 1fr 1fr 1fr 30px" spacing={10} mb={2}>
    <Heading as="h6" size="xs">
      Label
    </Heading>
    <Heading as="h6" size="xs">
      Price
    </Heading>
    <Heading as="h6" size="xs">
      Quantity
    </Heading>
    <Heading as="h6" size="xs">
      Total
    </Heading>
  </SimpleGrid>
)

export default HeaderLines
