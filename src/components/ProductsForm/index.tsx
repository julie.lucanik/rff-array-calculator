import React from 'react'
import { getIn } from 'final-form'
import arrayMutators from 'final-form-arrays'
import createDecorator from 'final-form-calculate'
import { Form, Field } from 'react-final-form'
import { FieldArray } from 'react-final-form-arrays'

import products from 'mocks/products.json'

import {
  createStandaloneToast,
  Input,
  InputGroup,
  InputRightAddon,
  SimpleGrid,
  Box,
  Button,
  HStack,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInputStepper,
  NumberInputField,
  NumberInput,
  IconButton,
} from '@chakra-ui/react'

import { AddIcon, DeleteIcon } from '@chakra-ui/icons'
import { CurrencyField } from 'components/Inputs'
import HeaderLines from './HeaderLines'

const ProductsForm = () => {
  const initialValues = {
    name: '',
    products: products.data,
    totalAmounts: 0,
  }

  const calculator = createDecorator(
    {
      field: /products\[\d+\]\.price/,
      updates: (value, name, allValues) => {
        const totalField = name.replace('.price', '.total')
        const quantityField = name.replace('.price', '.quantity')

        return {
          [totalField]: value * getIn(allValues, quantityField),
        }
      },
    },
    {
      field: /products\[\d+\]\.quantity/,
      updates: (value, name, allValues) => {
        const totalField = name.replace('.quantity', '.total')
        const priceField = name.replace('.quantity', '.price')

        return {
          [totalField]: value * getIn(allValues, priceField),
        }
      },
    },
    {
      field: 'products',
      updates: value => {
        const totalAmounts = value.reduce(
          (acc, current) => acc + current.total,
          0,
        )

        return {
          totalAmounts,
        }
      },
    },
  )

  const addProduct = fields => {
    const index = fields.value.length
    fields.push({ id: index, label: '', price: 0, quantity: 1, total: 0 })
  }

  const onSubmit = formValues => {
    // eslint-disable-next-line no-console
    console.info(formValues)
    const toast = createStandaloneToast()

    toast({
      title: 'Your form has been updated',
      status: 'success',
      variant: 'left-accent',
      duration: 2000,
    })
  }

  return (
    <Form
      initialValues={initialValues}
      subscription={{}}
      decorators={[calculator]}
      mutators={{ ...arrayMutators }}
      onSubmit={onSubmit}
    >
      {({ handleSubmit }) => (
        <form onSubmit={handleSubmit}>
          <Field
            name="name"
            render={({ input: { onChange, value } }) => (
              <Input
                size="sm"
                onChange={onChange}
                value={value}
                placeholder="Store name"
                isRequired
              />
            )}
          />

          <FieldArray name="products">
            {({ fields }) => (
              <Box my={8}>
                <HeaderLines />
                {fields.map((name: string, index: number) => (
                  <SimpleGrid
                    templateColumns="2fr 1fr 1fr 1fr 30px"
                    spacing={10}
                    key={name}
                    mb={2}
                  >
                    <Field
                      name={`${name}.label`}
                      render={({ input: { onChange, value } }) => (
                        <Input
                          size="sm"
                          placeholder="Label"
                          {...{ onChange, value }}
                        />
                      )}
                    />
                    <Field
                      name={`${name}.price`}
                      component={CurrencyField}
                      size="sm"
                      textAlign="right"
                    />
                    <Field
                      name={`${name}.quantity`}
                      render={({ input: { value, onChange } }) => (
                        <NumberInput {...{ value, onChange }} size="sm">
                          <NumberInputField textAlign="right" />
                          <NumberInputStepper>
                            <NumberIncrementStepper />
                            <NumberDecrementStepper />
                          </NumberInputStepper>
                        </NumberInput>
                      )}
                    />
                    <Field
                      name={`${name}.total`}
                      component={CurrencyField}
                      size="sm"
                      textAlign="right"
                      isReadOnly
                    />
                    <IconButton
                      colorScheme="red"
                      aria-label="delete product"
                      icon={<DeleteIcon />}
                      size="sm"
                      variant="ghost"
                      onClick={() => fields.remove(index)}
                    />
                  </SimpleGrid>
                ))}
                <HStack justify="flex-end" mt={4}>
                  <Button
                    leftIcon={<AddIcon />}
                    variant="ghost"
                    onClick={() => addProduct(fields)}
                    colorScheme="twitter"
                    size="sm"
                  >
                    product
                  </Button>
                </HStack>
              </Box>
            )}
          </FieldArray>
          <HStack justify="flex-end">
            <Field
              name="totalAmounts"
              size="sm"
              readOnly
              render={props => (
                <InputGroup size="sm" w={1 / 4}>
                  {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                  <CurrencyField {...props} />
                  <InputRightAddon>Product&apos;s total amount</InputRightAddon>
                </InputGroup>
              )}
            />
          </HStack>
          <HStack as="footer" justify="flex-end" my={8}>
            <Button type="submit" colorScheme="whatsapp">
              Submit
            </Button>
          </HStack>
        </form>
      )}
    </Form>
  )
}

export default ProductsForm
