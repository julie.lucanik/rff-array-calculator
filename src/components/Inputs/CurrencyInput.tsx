/* eslint-disable react/jsx-props-no-spreading */
import { useCurrencyFormat } from 'hooks/useCurrencyFormat'
import React from 'react'
import { FieldRenderProps } from 'react-final-form'
import NumberFormat, { NumberFormatProps } from 'react-number-format'
import { Input } from '@chakra-ui/react'

type Props = {
  label?: string
  decimalScale?: number
}
type CurrencyFieldProps = Omit<
  FieldRenderProps<number>,
  keyof NumberFormatProps
> &
  Props &
  NumberFormatProps

export const CurrencyField: React.FC<CurrencyFieldProps> = props => {
  const { input, meta, decimalScale, ...rest } = props
  const { value, onChange, ...otherInput } = input
  const currencyFormat = useCurrencyFormat({ decimalScale })

  return (
    <NumberFormat
      {...otherInput}
      {...currencyFormat}
      {...{ meta, ...rest }}
      value={value || 0}
      onValueChange={({ floatValue }) => {
        onChange(floatValue)
      }}
      customInput={Input}
    />
  )
}
