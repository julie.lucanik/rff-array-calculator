/* eslint-disable react/jsx-props-no-spreading */
import React from 'react'
import { Input } from '@chakra-ui/react'
import { FieldRenderProps } from 'react-final-form'
import NumberFormat, { NumberFormatProps } from 'react-number-format'

import { useNumberFormat } from 'hooks/useNumberFormat'

type Props = {
  label?: string
  unityDecimal?: number
}
type NumberFieldProps = Omit<
  FieldRenderProps<number>,
  keyof NumberFormatProps
> &
  Props &
  NumberFormatProps

export const NumberField: React.FC<NumberFieldProps> = props => {
  const { input, meta, ...rest } = props
  const { value, onChange, ...otherInput } = input
  const numberFormat = useNumberFormat()

  return (
    <NumberFormat
      {...otherInput}
      {...numberFormat}
      {...{ meta, ...rest }}
      value={value || 0}
      onValueChange={({ floatValue }) => {
        onChange(floatValue)
      }}
      customInput={Input}
    />
  )
}
