import * as React from 'react'
import { Global, css } from '@emotion/react'
import { ChakraProvider, Box, Heading } from '@chakra-ui/react'

import ProductsForm from 'components/ProductsForm'

const GlobalStyles = css`
  /*
    This will hide the focus indicator if the element receives focus via the mouse,
    but it will still show up on keyboard focus.
    https://medium.com/@keeganfamouss/accessibility-on-demand-with-chakra-ui-and-focus-visible-19413b1bc6f9
  */
  .js-focus-visible :focus:not([data-focus-visible-added]) {
    outline: none;
    box-shadow: none;
  }
`

function App() {
  return (
    <ChakraProvider>
      <Global styles={GlobalStyles} />
      <Box p={8} w="100vw" h="100vh" overflow="hidden">
        <Heading mb={4} as="h2">
          Final form array&apos;s calculator
        </Heading>
        <ProductsForm />
      </Box>
    </ChakraProvider>
  )
}
export default App
